FROM ubuntu:16.04

ARG PYPI_USER
ARG PYPI_PASSWORD
RUN apt-get update && \
   DEBIAN_FRONTEND=noninteractive apt-get -y install \
      git docker.io python3 python3-dev python3-pip && \
    rm -rf /var/lib/apt/lists/*

RUN ln -s -f /usr/bin/python3 /usr/bin/python
RUN ln -s -f /usr/bin/pip3 /usr/bin/pip

RUN pip3 install twine awscli
RUN mkdir /project

COPY _buildPythonPackage.sh /scripts/

ENTRYPOINT ["/scripts/_buildPythonPackage.sh"]