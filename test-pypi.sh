#!/usr/bin/env bash

docker build -t osimis/python-packager .

# docker run --rm --env PYPI_USER=$PYPI_USER --env PYPI_PASSWORD=$PYPI_PASSWORD -v /home/amazy/python-osimis-timer:/project osimis/python-packager osimis_timer

containerId=$(docker create --env PYPI_USER=$PYPI_USER --env PYPI_PASSWORD=$PYPI_PASSWORD osimis/python-packager osimis_timer am-ci)
docker cp /home/amazy/python-osimis-timer/. $containerId:/project
docker start -a $containerId
docker rm $containerId