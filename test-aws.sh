#!/usr/bin/env bash

docker build -t osimis/python-packager .

# docker run --rm --env PYPI_USER=$PYPI_USER --env PYPI_PASSWORD=$PYPI_PASSWORD -v /home/amazy/python-osimis-timer:/project osimis/python-packager osimis_timer
cd /home/amazy/interhosp/apps/restApi/sites/interfaces
gitBranchName=$(git rev-parse --abbrev-ref HEAD)
gitCommitSpec=$(git describe --long --dirty)
versionTag=$(git describe --tags --abbrev=0)
containerId=$(docker create --env AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID --env AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY --env AWS_URL=s3://orthanc.osimis.io/osimis-timer/  osimis/python-packager interhosp_interfaces $gitBranchName $gitCommitSpec $versionTag)
#docker cp /home/amazy/python-osimis-timer/. $containerId:/project
docker cp /home/amazy/interhosp/apps/restApi/sites/interfaces/. $containerId:/project

docker start -a $containerId
docker rm $containerId