#!/usr/bin/env bash
# script to be called from inside the Docker container to build the python package in /project

set -x # Trace execution
set -e # Stop on error

# start from the right place
cd /project
echo $PYPI_USER
if [[ -z $PYPI_USER ]]; then
	publishToPypi=false
else
	publishToPypi=true
fi

packageName=$1
branchName=${2:-$(git rev-parse --abbrev-ref HEAD)}
gitCommitSpec=${3:-$(git describe --long --dirty)} || true
versionTag=${4:-$(git describe --tags --abbrev=0)}  # only get the tag !
IS_OFFICIAL_RELEASE=false


# Tag the image with a more convenient tag that carries some semantics
# (like the branch name or the version number).
if [[ $branchName == master ]]; then
	# In the master branch, make sure that the tag is clean
	# ('1.2.3'; not 1.2.3-alpha) and that there have been 0 commits
	# since the tag has been set
	if [[ $gitCommitSpec =~ [0-9]+.[0-9]+.[0-9]+-0-g[0-9a-f]{7}$ ]]; then
        IS_OFFICIAL_RELEASE=true
	else
		echo "Invalid tag on the master branch.  Make sure" \
			"you have just tagged the master branch with" \
			"something like '1.2.3' and that there has" \
			"been no commits after the tag."
		exit 2
	fi
fi

if [[ $gitCommitSpec =~ [0-9]+.[0-9]+.[0-9]+-([0-9]+)-g([0-9a-f]{7}) ]]; then
	commitNumber="${BASH_REMATCH[1]}"
	commitSha1="${BASH_REMATCH[2]}"
fi

# update version number in setup.py
if [[ $publishToPypi = true ]]; then
	# for pypi, we need 3 digit version numbers
	sed -i "s/0.0.0/$versionTag/g" setup.py
else
	# for AWS, we usually use longer version numbers
	sed -i "s/'0.0.0-0-000000'/'$versionTag-$commitNumber-$commitSha1'/g" setup.py
	# but we might also use 3 digit version numbers
	sed -i "s/0.0.0/$versionTag/g" setup.py
fi


# build the package
python setup.py sdist bdist_wheel

# test the package
if [[ ! -e /project/$packageName/tests/tests.py ]]; then
	echo "no tests found !"
else
	if [[ -e /project/requirements.txt ]]; then
		pip install -r /project/requirements.txt
	fi
	PYTHONPATH=/project python -m unittest $packageName/tests/tests.py -v
fi


if [[ $publishToPypi = true ]]; then
	if [[ $IS_OFFICIAL_RELEASE = true ]]; then
	    # upload to pypi

	    twine register $packageName  -u $PYPI_USER -p $PYPI_PASSWORD || true  # it will succeed only the first time ...
	    twine upload dist/$packageName-$versionTag.tar.gz -u $PYPI_USER -p $PYPI_PASSWORD
	else
	    # upload to pypitest

	    twine register -r pypitest $packageName  -u $PYPI_USER -p $PYPI_PASSWORD || true  # it will succeed only the first time ...
	    twine upload -r pypitest dist/$packageName-$versionTag.tar.gz -u $PYPI_USER -p $PYPI_PASSWORD

	    # to install from pypitest:
	    # pip install -i https://testpypi.python.org/pypi osimis_timer
	fi
else
	# upload to AWS
	aws s3 --region eu-west-1 cp $(find /project/dist/*.tar.gz) $AWS_URL

fi
