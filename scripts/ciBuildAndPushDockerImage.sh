#!/usr/bin/env bash
# Builds the Osimis License Generator Docker image
# and pushes it to Docker repository if the branch name is master, and a suitable git tag is found
# Arguments:
#   $1 - Git branch name

set -x # Trace execution
set -e # Stop on error
set -u # Stop on uninitialized variable

cd "${REPOSITORY_PATH:-$(git rev-parse --show-toplevel)}"/

branchName=${1:-$(git rev-parse --abbrev-ref HEAD)} #if no argument defined, get the branch name from git

gitLongTag=$(git describe --long --dirty)

imageName=

if [[ $branchName == "master" ]]; then
	
	# in the master branch, make sure the tag is clean ('1.2.3'; not 1.2.3-alpha) and there has been 0 commits since the tag has been set.
	if [[ $gitLongTag =~ ^(([[:digit:]]+(\.[[:digit:]]+)*)(-((-|[[:alnum:]])+))*)-0-g[a-f0-9]+$  ]]; then 

		releaseTag=${BASH_REMATCH[1]}
	else

		echo "Invalid tag on the master branch.  Make sure you have just tagged the master branch with something like '1.2.3' and that there has been no commit after the tag."
		exit -1	
	fi

else
	# in other branches than master, the versionNumber is the branchName
	releaseTag=$branchName
	
	# if the branch name is something like 'am/WVB-27', the image tag should be 'am-WVB-27'
	# replace / by -
	releaseTag=${releaseTag//\//-}
fi


docker build --tag "osimis/python-packager:$releaseTag" .
docker push "osimis/python-packager:$releaseTag"

if [[ $branchName == "master" ]]; then

	docker tag "osimis/python-packager:$releaseTag" "osimis/python-packager:latest"
	docker push "osimis/python-packager:latest"
fi