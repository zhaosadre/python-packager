Builds a python package and publishes it to pypi or AWS.  

Example usage to push a simple package to PYPI:
----------------------------------------------
```
docker run --rm --env PYPI_USER=$PYPI_USER --env PYPI_PASSWORD=$PYPI_PASSWORD -v /home/amazy/python-osimis-timer:/project osimis/python-packager osimis_timer
```

or, in a Jenkinsfile:
--------------------

```
#!/bin/bash
set -e
set -x

# start from the right place
cd "${REPOSITORY_PATH:-$(git rev-parse --show-toplevel)}"/

branchName=${1:-$(git rev-parse --abbrev-ref HEAD)}

containerId=$(docker create --env PYPI_USER=$PYPI_USER --env PYPI_PASSWORD=$PYPI_PASSWORD osimis/python-packager:0.1.0 osimis_timer $branchName)
docker cp $(pwd)/. $containerId:/project
docker start -a $containerId
docker rm $containerId
```

When publishing to PYPI, you're expected to have a version number like this one in your `setup.py` file:
```
setup(
    ...
    version='0.0.0',  # always keep all zeroes version, it's updated by the CI script
    ...
)
```

Example usage to push a simple package to AWS:
---------------------------------------------

```
cd /home/amazy/interhosp/apps/restApi/sites/interfaces
gitBranchName=$(git rev-parse --abbrev-ref HEAD)
gitCommitSpec=$(git describe --long --dirty)
versionTag=$(git describe --tags --abbrev=0)

containerId=$(docker create --env AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID --env AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY --env AWS_URL=s3://orthanc.osimis.io/osimis-timer/  osimis/python-packager interhosp_interfaces $gitBranchName $gitCommitSpec $versionTag)
docker cp /home/amazy/python-osimis-timer/. $containerId:/project

docker start -a $containerId
docker rm $containerId
```

When publishing to AWS, you're expected to have a version number like this one in your `setup.py` file:
```
setup(
    ...
    version = '0.0.0-0-000000', # this is updated in the CI script (never commit if version number is different from '0.0.0-0-000000')
    ...
)
```
